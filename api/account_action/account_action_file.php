<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/11/21 0021
 * Time: 10:00
 */
$version = $params['version'] ?? 'v1';  //判断版本类型
$account_action_file_array['userLogin'] = "api/account_action/$version/userLogin.php";
$account_action_file_array['userRegister'] = "api/account_action/$version/userRegister.php";