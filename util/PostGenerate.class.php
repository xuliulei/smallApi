<?php
/**
 * 根据不同的动作生成不同的post
 *
 */
namespace Util;

 class PostGenerate{
    public static function generateTransferOriginatorPost($originatorId,$originatorName){
        return "*$originatorName*|*/user/user-detail/$originatorId*|我把创始人身份移交给%s";
    }

    public static function generateRescindAdminPost($adminId,$adminName){
        return "*$adminName*|*/user/user-detail/$adminId*|我移除了%s的管理员权限";
    }

    public static function generateAddAdminPost($adminId,$adminName){
        return "*$adminName*|*/user/user-detail/$adminId*|我添加了管理员%s";
    }

    public static function generateAddPhotoPost($personId,$personName){
        return "*$personName*|*/person/person-detail/$personId*|我为%s添加了一张照片";
    }

    public static function generateAddRelatePersonPost($personId,$personName){
        return "*$personName*|*/person/person-detail/$personId*|我向家族新增了一个人物%s";
    }

    public static function generateUpdatePersonPost($personId,$personName){
        return "*$personName*|*/person/person-detail/$personId*|我更新了%s的人物资料";
    }

    public static function generateDeletePersonPost($personId,$personName){
        return "我删除了人物$personName";
    }

    public static function generateAddEventPost($familyId,$personId,$eventId,$eventName){
        return "*$eventName*|*/event/event-detail/$familyId/$personId/$eventId*|我添加了大事件%s";
    }

    public static function generateUpdateEventPost($familyId,$personId,$eventId,$eventName){
        return "*$eventName*|*/event/event-detail/$familyId/$personId/$eventId*|我更新了大事件%s";
    }

    public static function generateDeleteEventPost($eventName){
        return "我删除了大事件《{$eventName}》";
    }

    public static function generateAddActivityPost($activityId,$title){
        return "*$title*|*/activity/activity-detail/$activityId*|我发起了活动%s,快来看看吧!";
    }

    public static function generateUpdateActivityPost($activityId,$title){
        return "*$title*|*/activity/activity-detail/$activityId*|我更新了活动内容%s,快来看看吧!";
    }
 }