<?php

    namespace Util;

    //需要更新的人物关系的对应关系
    class PersonRecord{
        var $id = 0;			//需要更新的记录id
        var $field = "";		//需要更新记录中的字段
        var $targetId = 0;   	//更新的内容
        
        public function __construct($id,$field,$targetId){
            $this->id = $id;
            $this->field = $field;
            $this->targetId = $targetId;
        }
        
        public function clean(){
            $this->id = 0;
            $this->field = "";
            $this->targetId = 0;
        }
    };
?>