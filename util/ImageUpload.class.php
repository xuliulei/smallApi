<?php
	/**
	 * 即将弃用
	 *
	 */
	namespace Util;
	use Util\Util;

	class ImageUpload{
		private $file;
		private $path;
		private $imgTypeArray;
		private $imagick;
		function __construct($tmpFile,$path){
			$this->file=$tmpFile;
			//$_SERVER['DOCUMENT_ROOT']."/images/upload/"
			if(!file_exists($_SERVER['DOCUMENT_ROOT'].$path)){
				mkdir($_SERVER['DOCUMENT_ROOT'].$path,0755);
			}
			$this->path=$path.time().Util::generateRandomCode(24);
			$this->imgTypeArray=array("image/gif","image/jpeg","image/jpeg","image/png");
			$this->imagick =  new \Imagick($this->file["tmp_name"]);
		}
		public function getPath(){
			return $this->path;
		}
		public function checkType($_imgTypeArray = null){
			if($_imgTypeArray!=null){
				$this->imgTypeArray=$_imgTypeArray;
			}
			for($i=0;$i<count($this->imgTypeArray);$i++){
				if(in_array($this->file['type'],$this->imgTypeArray)){
					return true;
				}else{
					return false;
				}
			}
		}
		public function checkSize($limit){
			if($this->file['size']<=$limit){
				return true;
			}else{
				return false;
			}
		}

		/**
		 * 文件上传
		 * @param $noCompress true不压缩,false压缩,默认false
		 * @return int false是上传失败,true是上传成功
		 */
		public function upload($noCompress = false){

			if(!$this->checkType()){		//检查文件格式
				return false;
			}					

			if(!$this->checkSize(2 * 1024 * 1024)){	//限制在2M之内
				return false;
			} 	
			
			if (isset($this->file["error"]) && $this->file["error"] > 0)
			{
				return false;
			}
			if (isset($this->file["name"]) && file_exists($this->path))
			{
				return false;
			}
			else
			{	
				if(!$noCompress)
					$this->compress();		//默认对所有图片进行压缩
				if(file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->path,$this->imagick->getImageBlob()))
					return true;
				else
					return false;
			}
		}

		/**
		 * 对图片进行压缩
		 */
		public function compress(){

		}


		/**
		 * 生成缩略图
		 * @param $columns 宽度
		 * @param $rows    高度
		 * @param $bestfit 最大适应宽度,
		 * @param $fill 
		 * @param $replace 是否替换原图存储
		 */
		public function thumbnail($columns,$rows,$bestfit = false ,$fill = false,$replace = false){
			$this->imagick->thumbnailImage($columns, $rows, $bestfit, $fill);
			if(!$replace){
				file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->path.'_thumb',$this->imagick->getImageBlob());
			}
		}

		/**
		 * 图片裁剪工具,
		 * @param $x 裁剪的左上角横坐标
		 * @param $y 裁剪的左上角纵坐标
		 * @param $width 裁剪的宽度
		 * @param $width 裁剪的高度
		 */
		public function crop($x,$y,$width,$height){
			$this->imagick->cropImage($width,$height,$x,$y);
		}

		public function cropDefault(){
			$d = $this->imagick->getImageGeometry();
			$width = $d['width'];
			$height = $d['height'];

			if($width > $height){
				$x = ($width - $height) / 2;
				$y = 0;
				$width = $height;
			}else if($height > $width){
				$x = 0;
				$y = 0;
				$height = $width;
			}else{
				$x = 0;
				$y = 0;
			}

			$this->crop($x,$y,$width,$height);
		}
		
	};
	?>