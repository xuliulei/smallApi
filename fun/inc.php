<?php
/**
 * Info:
 * User: xuliulei
 */

/**
 * 控制路径 文件名称  上传文件类型格式  控制文件大小写
 * @param $path
 */
function upload($path, $rename = 0,$size=1024*1024*2,$allow=['jpg','png','gif','rar','zip','doc','xls']) {
    $ups = [];
    if (!file_exists($path)) mkdir($path, 0777, true);
    foreach ($_FILES as $file) {
        if (is_array($file['tmp_name'])) {
            foreach ($file['tmp_name'] as $key => $v) {
                if (is_uploaded_file($file['tmp_name'][$key]) && $file['size'][$key]<$size) {
                    $fn = $file['name'][$key];
                    if(!in_array(strtolower(pathinfo($fn,PATHINFO_EXTENSION)),$allow)) continue;
                    $fn = name($path . $fn, $rename);
                    move_uploaded_file($file['tmp_name'][$key], $path . $fn);
                    $ups[] = $fn;
                }
            }
        } else {
            if (is_uploaded_file($file['tmp_name']) && $file['size']<$size) {
                $fn = $file['name'];
                if(!in_array(strtolower(pathinfo($fn,PATHINFO_EXTENSION)),$allow)) continue;
                $fn = name($path . $fn, $rename);
                move_uploaded_file($file['tmp_name'], $path . $fn);
                $ups[] = $fn;
            }
        }
    }
    if (count($ups) == 1) $ups = $ups[0];
    if(count($ups)==0) return null;
    return $ups;
}

/**
 * 0 原名  1 YmdHis_6随机数.扩展名   2 唯一  3 uuid  4原名加编号
 * @param $fn
 * @param int $rename
 */
function name($fn, $rename = 0) {
    switch ($rename) {
        case 0:
            $fn = pathinfo($fn, PATHINFO_BASENAME);
            break;
        case 1:
            $fn = sprintf('%s_%05d.%s', date('YmdHis'), mt_rand(0, 99999), pathinfo($fn, PATHINFO_EXTENSION));
            break;
        case 2:
            $fn = sprintf('%s.%s', uniqid(), pathinfo($fn, PATHINFO_EXTENSION));
            break;
        case 3:
            $fn = sprintf('%s.%s', uuid(), pathinfo($fn, PATHINFO_EXTENSION));
            break;
        default:
            $path = pathinfo($fn,PATHINFO_DIRNAME).'/';
            $fnn = pathinfo($fn,PATHINFO_FILENAME);
            $ext = pathinfo($fn,PATHINFO_EXTENSION);
            $fn = pathinfo($fn,PATHINFO_BASENAME);
            $n = 0;
            while(file_exists($path.$fn)){
                $fn = sprintf('%s(%d).%s',$fnn,++$n,$ext);
            }
            break;
    }
    return strtolower($fn);
}

/**
 * <p>功能:</p>
 * @param string $namespace
 * @return string
 */
function uuid($namespace = '') {
    static $guid = '';
    $uid = uniqid("", true);
    $data = $namespace;
    $data .= $_SERVER['REQUEST_TIME'];
    $data .= $_SERVER['HTTP_USER_AGENT'];
    if(isset($_SERVER['LOCAL_ADDR'])) $data .= $_SERVER['LOCAL_ADDR'];
    if(isset($_SERVER['LOCAL_PORT'])) $data .= $_SERVER['LOCAL_PORT'];
    if(isset($_SERVER['SERVER_ADDR'])) $data .= $_SERVER['SERVER_ADDR'];
    if(isset($_SERVER['SERVER_PORT'])) $data .= $_SERVER['SERVER_PORT'];
    $data .= $_SERVER['REMOTE_ADDR'];
    $data .= $_SERVER['REMOTE_PORT'];
    $hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
    $guid = substr($hash, 0, 8) . '-' . substr($hash, 8, 4) . '-' . substr($hash, 12, 4) . '-' . substr($hash, 16, 4) . '-' . substr($hash, 20, 12);
    return strtolower($guid);
}


function c($c = 'rand', $a = 0) {
    global $i;
    switch ($c) {
        case 'white':
            $color = imagecolorallocatealpha($i, 255, 255, 255, $a);
            break;
        case 'black':
            $color = imagecolorallocatealpha($i, 0, 0, 0, $a);
            break;
        case 'red':
            $color = imagecolorallocatealpha($i, 255, 0, 0, $a);
            break;
        case 'green':
            $color = imagecolorallocatealpha($i, 0, 255, 0, $a);
            break;
        case 'blue':
            $color = imagecolorallocatealpha($i, 0, 0, 255, $a);
            break;
        case 'yellow':
            $color = imagecolorallocatealpha($i, 255, 255, 0, $a);
            break;
        case 'orange':
            $color = imagecolorallocatealpha($i, 254, 158, 52, $a);
            break;
        case 'purple':
            $color = imagecolorallocatealpha($i, 193, 19, 172, $a);
            break;
        case 'gray':
            $color = imagecolorallocatealpha($i, 166, 166, 166, $a);
            break;
        case 'rand':
            $color = imagecolorallocatealpha($i, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255), $a);
            break;
        default:
            $cs = str_split($c, 2);
            $color = imagecolorallocatealpha($i, hexdec($cs[0]), hexdec($cs[1]), hexdec($cs[2]), $a);
            break;
    }
    return $color;
}

function thumb($img, $w = 200, $h = 0, $dir = 'small') {
    $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
    if ($ext == 'jpg') {
        $i = imagecreatefromstring(file_get_contents($img));
        $ww = imagesx($i);
        $hh = imagesy($i);
        if ($h == 0) {
            //按比较缩略
            if ($w < 1) {
                $dw = $ww * $w;
            } else {
                $dw = $w;
            }
            $dh = $dw / $ww * $hh;
        } else {
            $dw = $w;
            $dh = $h;
        }
        $di = imagecreatetruecolor($dw, $dh);
        imagecopyresampled($di, $i, 0, 0, 0, 0, $dw, $dh, $ww, $hh);

        $path = pathinfo($img, PATHINFO_DIRNAME) . '/' . $dir . '/';
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $dimg = $path . pathinfo($img, PATHINFO_BASENAME);
        imagejpeg($di, $dimg, 80);
        imagedestroy($di);
        imagedestroy($i);
    }
}

function logo($img, $pos = 5) {
    $i = imagecreatefromstring(file_get_contents($img));
    $w = imagesx($i);
    $h = imagesy($i);
    $logo = imagecreatefromstring(file_get_contents(dirname(__FILE__) . '/logo.png'));
    $ww = imagesx($logo);
    $hh = imagesy($logo);
    switch ($pos) {
        case 1:
            $x = 10;
            $y = 10;
            break;
        case 2:
            $x = ($w - $ww) / 2;
            $y = 10;
            break;
        case 3:
            $x = $w - $ww - 10;
            $y = 10;
            break;
        case 4:
            $x = 10;
            $y = ($h - $hh) / 2;
            break;
        case 5:
            $x = ($w - $ww) / 2;
            $y = ($h - $hh) / 2;
            break;
        case 6:
            $x = $w - $ww - 10;
            $y = ($h - $hh) / 2;
            break;
        case 7:
            $x = 10;
            $y = $h - $hh - 10;
            break;
        case 8:
            $x = ($w - $ww) / 2;
            $y = $h - $hh - 10;
            break;
        case 9:
            $x = $w - $ww - 10;
            $y = $h - $hh - 10;
            break;
        default:
            $x = mt_rand(10, $w - $ww - 10);
            $y = mt_rand(10, $h - $hh - 10);
            break;
    }
    imagecopy($i, $logo, $x, $y, 0, 0, $ww, $hh);
    imagejpeg($i, $img, 80);
    imagedestroy($i);
    imagedestroy($logo);
}

function text($srcdir, $text = '版权所有 盗版必究', $size = 25, $color = 'rand', $ps = 5, $dstdir = './tmp/') {
    global $i;
    //建立目标目录
    $dir = $dstdir;
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true);
    }

    if (is_dir($srcdir)) {
        $fs = array_diff(scandir($srcdir), ['.', '..']);
        foreach ($fs as $f) {
            if (is_dir("$srcdir/$f")) {
                text("$srcdir/$f", $text, $size, $color, $ps, $dstdir);
            }
            $ext = strtolower(pathinfo($f, PATHINFO_EXTENSION));
            $allow = ['jpg', 'png', 'gif'];
            if (in_array($ext, $allow)) {
                $ff = "$srcdir/$f";
                $i = imagecreatefromstring(file_get_contents($ff));
                $w = imagesx($i); //取得宽度
                $h = imagesy($i);//取得高度

                $angle = 0;
                $cr = c($color, 50);
                $fontfile = dirname(__FILE__) . '/font/0.ttf';
                $pos = imagettfbbox($size, $angle, $fontfile, $text);
                switch ($ps) {
                    case 1:
                        $x = 10;
                        $y = 10 - $pos[5];
                        break;
                    case 2:
                        $x = ($w - $pos[2]) / 2;
                        $y = 10 - $pos[5];
                        break;
                    case 3:
                        $x = $w - $pos[2] - 10;
                        $y = 10 - $pos[5];
                        break;
                    case 4:
                        $x = 10;
                        $y = ($h + $pos[5]) / 2 - ($pos[5] / 2);
                        break;
                    case 5:
                        $x = ($w - $pos[2]) / 2;
                        $y = ($h + $pos[5]) / 2 - ($pos[5] / 2);
                        break;
                    case 6:
                        $x = $w - $pos[2] - 10;
                        $y = ($h + $pos[5]) / 2 - ($pos[5] / 2);
                        break;
                    case 7:
                        $x = 10;
                        $y = $h - 10;
                        break;
                    case 8:
                        $x = ($w - $pos[2]) / 2;
                        $y = $h - 10;
                        break;
                    case 9:
                        $x = $w - $pos[2] - 10;
                        $y = $h - 10;
                        break;
                    default:
                        $x = mt_rand(10, $w - $pos[2] - 10);
                        $y = mt_rand(10, $h - 10);
                        break;
                }
                imagettftext($i, $size, $angle, $x, $y, $cr, $fontfile, $text);
                imagejpeg($i, $dir . $f, 80);
                imagedestroy($i);
            }

        }
    }

    if (is_file($srcdir)) {
        $ext = strtolower(pathinfo($srcdir, PATHINFO_EXTENSION));
        $allow = ['jpg', 'png', 'gif'];
        if (in_array($ext, $allow)) {
            $ff = "$srcdir";
            $i = imagecreatefromstring(file_get_contents($srcdir));
            $w = imagesx($i); //取得宽度
            $h = imagesy($i);//取得高度

            $angle = 0;
            $color = c($color, 50);
            $fontfile = dirname(__FILE__) . '/font/0.ttf';
            $pos = imagettfbbox($size, $angle, $fontfile, $text);
            switch ($ps) {
                case 1:
                    $x = 10;
                    $y = 10 - $pos[5];
                    break;
                case 2:
                    $x = ($w - $pos[2]) / 2;
                    $y = 10 - $pos[5];
                    break;
                case 3:
                    $x = $w - $pos[2] - 10;
                    $y = 10 - $pos[5];
                    break;
                case 4:
                    $x = 10;
                    $y = ($h + $pos[5]) / 2;
                    break;
                case 5:
                    $x = ($w - $pos[2]) / 2;
                    $y = ($h + $pos[5]) / 2;
                    break;
                case 6:
                    $x = $w - $pos[2] - 10;
                    $y = ($h + $pos[5]) / 2;
                    break;
                case 7:
                    $x = 10;
                    $y = $h - 10;
                    break;
                case 8:
                    $x = ($w - $pos[2]) / 2;
                    $y = $h - 10;
                    break;
                case 9:
                    $x = $w - $pos[2] - 10;
                    $y = $h - 10;
                    break;
                default:
                    $x = mt_rand(10, $w - $pos[2] - 10);
                    $y = mt_rand(10, $h - 10);
                    break;
            }
            imagettftext($i, $size, $angle, $x, $y, $color, $fontfile, $text);
            imagejpeg($i, $dir . pathinfo($srcdir, PATHINFO_BASENAME), 80);
            imagedestroy($i);
        }
    }
}

/**
 * @param $ip
 * @return string
 */
function convertIpToString($ip){
    $long = 4294967295 - ($ip - 1);
    return long2ip(-(int)$long);
}

/**
 * @param $ip
 * @return string
 */
function convertIpToLong($ip){
    return sprintf("%u", ip2long($ip));
}