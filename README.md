**SIMPLE-API**
简单的api接口
````````````````````
入口文件:index.php

PS:需要注意一下！

#生成环境下使用
composer dump-atoload -o
#这里的意思是composer dump-autoload --optimize，不是用的话会损失性能。

 "autoload":{
    //命名空间 路径
        "psr-4":{
            "DB\\":"db/",
            "Util\\":"util/"
        },
        "files":[
        //文件
            "config/db.php",
            "config/global.php"
        ]
    }
