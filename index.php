<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 2017/11/20 0020
 * Time: 18:42
 */

header('Access-Control-Allow-Origin:*'); //跨域
date_default_timezone_set("Asia/Shanghai"); //默认时间

define('SYSTEM_START_MEMORY', memory_get_usage()); //定义系统开始内存占用
define('SYSTEM_START_TIME', microtime(true)); //定义系统开始时间
require_once "vendor/autoload.php";
require_once "api/action_file.php";  //引入action_file.php 文件

use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\NativeMailerHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Util\Util;

$logger = new Logger('logger');
$logger->pushHandler(new NativeMailerHandler("1983766950@qq.com", "项目出现警告或者错误", "18937126028@163.com", logger::WARNING));
$year = date('Y');
$month = date('m');
$day = date('d');
$path = __DIR__ . "/log/$year/$month/$day/";
if (!file_exists($path)) {
    $dirs = explode('/', $path);
    $dir = '';
    foreach ($dirs as $part) {
        $dir .= $part . '/';
        if (!file_exists($dir) & !is_dir($dir) && strlen($dir) > 0)
            mkdir($dir, 0777);
    }

}

$logger->pushHandler(new StreamHandler($path . "/warningLogger.log", Logger::WARNING));
$logger->pushHandler(new FirePHPHandler());

$GLOBALS['logger'] = $logger;

$params = $_REQUEST; //请求得到参数 可以是GET POST 请求方式

$action = isset($params['action']) ? $params['action'] : 0;
if (!array_key_exists($action, $action_file_array)) {
    $errorCode = $GLOBALS['ERROR_REQUEST'];
    $data = "action not exists" ;
    Util::printResult($errorCode, $data);
    exit(-1);
}

$action_file = $action_file_array[$action];
if (!is_file($action_file)) {
    $errorCode = $GLOBALS['ERROR_REQUEST'];
    $data = "action file not exists" ;
    Util::printResult($errorCode, $data);
    exit(-1);
}

require_once( "$action_file");